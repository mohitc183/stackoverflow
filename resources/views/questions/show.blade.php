@extends('layouts.app')
@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.3.1/trix.css"></link>

    <!-- <link rel="stylesheet" type="text/css" href="trix.css"> -->
  
@endsection

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h1> {{ $question->title }} </h1>
                    </div>
                    <div class="card-body">
                        {!! $question->body !!}
                    </div>
                    <div class="card-footer">
                        <div class="d-flex justify-content-between mr-3">
                            <div class="d-flex">
                                <div>
                                    @can('vote', $question)
                                        <form action=" {{ route('questions.vote', [ $question->id, 1 ] ) }} " method="POST">
                                            @csrf
                                            @if(auth()->user()->hasQuestionUpVote($question))
                                                <button class="btn {{ auth()->user()->hasQuestionUpVote($question) ? 'text-dark' : 'text-black-50' }}" type="button">
                                                    <i class="fa fa-caret-up fa-3x" aria-hidden="true"></i>
                                                </button>
                                            @else
                                                <button class="btn {{ auth()->user()->hasQuestionUpVote($question) ? 'text-dark' : 'text-black-50' }}" type="submit">
                                                    <i class="fa fa-caret-up fa-3x" aria-hidden="true"></i>
                                                </button>
                                            @endif
                                        </form>
                                    @else
                                        <a href="{{ route('login') }}" title="Up Vote" class="vote-up d-block text-black-50">
                                            <i class="fa fa-caret-up fa-3x" aria-hidden="true"></i>
                                        </a>
                                    @endcan

                                    <h4 class="votes-count text-muted text-center m-0"> {{ $question->votes_count }} </h4>

                                    @can('vote', $question)
                                        <form action=" {{ route('questions.vote', [ $question->id, -1 ] ) }} " method="POST">
                                            @csrf
                                            @if(auth()->user()->hasQuestionDownVote($question))
                                                <button class="btn {{ auth()->user()->hasQuestionDownVote($question) ? 'text-dark' : 'text-black-50' }}" type="button">
                                                    <i class="fa fa-caret-down fa-3x" aria-hidden="true"></i>
                                                </button>
                                            @else
                                                <button class="btn {{ auth()->user()->hasQuestionDownVote($question) ? 'text-dark' : 'text-black-50' }}" type="submit">
                                                    <i class="fa fa-caret-down fa-3x" aria-hidden="true"></i>
                                                </button>
                                            @endif
                                        </form>
                                    @else
                                        <a href="{{ route('login') }}" title="Down Vote" class="vote-down d-block text-black-50">
                                            <i class="fa fa-caret-down fa-3x" aria-hidden="true"></i>
                                        </a>    
                                    @endcan 

                                </div>

                                <div class="ml-5 mt-2 {{ $question->is_favorite ? 'text-warning' : 'text-black-50' }} ">
                                    @can('markAsFavorite', $question)
                                        <form action="{{ route( $question->is_favorite ? 'questions.unfavorite' : 'questions.favorite', $question->id ) }}" method="POST">
                                            @csrf
                                            @if( $question->is_favorite )
                                                @method('DELETE')
                                            @endif
                                            <button class="btn {{ $question->is_favorite ? 'text-warning' : 'text-black-50' }}" type="submit">
                                                <i class="fa {{ $question->is_favorite ? 'fa-star' : 'fa-star-o' }} fa-2x"></i>
                                            </button>
                                            <h4 class="votes-count m-0 text-center">{{ $question->favorites_count }}</h4>
                                        </form>
                                    @else                                            
                                        <i class="fa fa-star-o fa-2x"></i>                               
                                        <h4 class="votes-count m-0 text-center">{{ $question->favorites_count }}</h4>
                                    @endcan
                                </div>
                            </div>

                            <div class="d-flex flex-column">
                                <div class="text-muted text-right mb-2">
                                    Questioned {{ $question->created_date }}
                                </div>
                                <div class="d-flex mb-2">
                                    <div class="">
                                        <img src="{{ $question->owner->avatar }}" alt="">
                                    </div>
                                    <div class="mt-2 ml-2">
                                        {{ $question->owner->name }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
        </div>

        <!-- Answers -->

        @include('answers._index')
        @include('answers._create')
    </div>

@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.3.1/trix.js"></script>
    

    <!-- <script type="text/javascript" src="trix.js"></script> -->
@endsection
