@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.3.1/trix.css"></link>

    <!-- <link rel="stylesheet" type="text/css" href="trix.css"> -->
  
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card"> 
                    <div class="card-header"><h3>Edit A Question</h3></div>
                    <div class="card-body">
                        <form action="{{ route('questions.update', $question->id) }}" method = 'POST'>
                            @csrf
                            @method("PUT")
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text"
                                        id="title"
                                        name ="title"
                                        placeholder="Enter Title"
                                        value="{{ old('title', $question->title) }}"
                                        class="form-control {{ $errors->has('title') ? 'is_invalid' : ' ' }}">
                                @error('title')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input type="hidden" id="body" name="body" value="{{ old('body', $question->body) }}">
                                <trix-editor input="body"></trix-editor>
                                @error('body')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <button class="btn btn-outline-success" type="submit" >Post a Question!</button>
                            </div>
                        </form>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.3.1/trix.js"></script>
    

    <!-- <script type="text/javascript" src="trix.js"></script> -->
@endsection