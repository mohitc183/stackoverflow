<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('/','App\Http\Controllers\QuestionsController' );

Auth::routes();

//Route::resource('questions', 'QuestionsController');

Route::get('users.notifications', 'App\Http\Controllers\UsersController@notifications' )->name('users.notifications');

Route::resource('questions', 'App\Http\Controllers\QuestionsController')->except('show');
Route::get('questions/{slug}', 'App\Http\Controllers\QuestionsController@show')->name('questions.show');

Route::post('questions/{question}/favorite', 'App\Http\Controllers\FavoritesController@store')->name('questions.favorite');
Route::delete('questions/{question}/unfavorite', 'App\Http\Controllers\FavoritesController@destroy')->name('questions.unfavorite');

Route::post('questions/{question}/vote/{vote}', 'App\Http\Controllers\VotesController@voteQuestion')->name('questions.vote');
Route::post('answers/{answer}/vote/{vote}', 'App\Http\Controllers\VotesController@voteAnswer')->name('answers.vote');

Route::resource('questions.answers', 'App\Http\Controllers\AnswersController')->except([ 'index', 'show', 'create' ]);
Route::put('answers/{answer}/best-answer', 'App\Http\Controllers\AnswersController@bestAnswer')->name('answers.bestAnswer');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');



