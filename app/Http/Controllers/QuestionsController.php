<?php

namespace App\Http\Controllers;

use App\Models\Question;
use Illuminate\Http\Request;
use App\Http\Requests\Questions\CreateQuestionRequest;
use App\Http\Requests\Questions\UpdateQuestionRequest;
use App\Policies\QuestionsPolicy;


class QuestionsController extends Controller
{
    public function __construct()
    {
        $this->middleware( 'auth' )->only( ['create', 'store', 'edit', 'update'] );  //will prevent a non logged-in user from creating and posting a question        
                                                                    //NOTE:- 'create' & 'store' are the methods declared below 

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $questions = Question::with('owner')
                                ->latest()
                                ->paginate(10); //eager load

        return view('questions.index' , compact([
            'questions'
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //app('debugbar')->disable();           <- to disable the debugbar on this page
        return view('questions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateQuestionRequest $request)
    {
        //
        auth()->user()->questions()->create( [
            'title' => $request->title,
            'body' => $request->body,
        ] );

        session()->flash( 'success', "Question has been added Successfully!!" );

        return redirect( route( 'questions.index' ) );

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Question  $question
     * @return \Illuminate\Http\Response
     */
    // public function show(Question $slug)
    // {
    //     //
    //     $slug = Question::where('slug', $slug)->firstOrFail();
    // }
    public function show(Question $question)
    {
        //
        $question->increment('views_count');

        return view('questions.show', compact( [
            'question',
        ] ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question)
    {
        //
        //using gates for authorization
        // if(Gate::allows('update-question', $question)){

        //     return view('questions.edit', compact( [
        //         'question'
        //     ] ));
        // }

        // abort(403);
        //using policy 'QuestionPolicy' in place of gate(s)

        
        if($this->authorize('update', $question)){

            return view('questions.edit', compact( [
                'question'
            ] ));
        }

        abort(403);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateQuestionRequest $request, Question $question)
    {
        //
        // if(Gate::allows('update-question', $question)){

        //     $question->update( [
        //         'title' => $request->title,
        //         'body'  => $request->body,
        //     ] );
    
        //     session()->flash( 'success', "Question has been updated Successfully!!" );
    
        //     return redirect( route( 'questions.index' ) );
        // };

        // abort(403);

        //using policy 'QuestionPolicy' in place of gate(s)
        if($this->authorize('update', $question)){

            $question->update( [
                'title' => $request->title,
                'body'  => $request->body,
            ] );
    
            session()->flash( 'success', "Question has been updated Successfully!!" );
    
            return redirect( route( 'questions.index' ) );
        };

        abort(403);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function destroy(Question $question)
    {
        //

        // if(auth()->user()->can('delete-question', $question)){      //this if statement is same as the if statement in update & edit methods but this increases the readability of the program i.e both statements use gates
        //     $question->delete();

        //     session()->flash('success', "Question has been Deleted Successfully!!");
        //     return redirect( route( 'questions.index' ) );
        // }
        // abort(403);


        //using policy 'QuestionPolicy in place of gate(s)

        if($this->authorize('delete', $question)){      //if using policy then authorize method is availible in question model

            $question->delete();

            session()->flash('success', "Question has been Deleted Successfully!!");
            return redirect( route( 'questions.index' ) );
        }
        abort(403);        
      
    }
}
