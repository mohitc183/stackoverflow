<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\Question;
use Illuminate\Http\Request;
use App\Http\Requests\CreateAnswerRequest;
use App\Http\Requests\UpdateAnswerRequest;
use App\Notifications\NewReplyAdded;

class AnswersController extends Controller
{
    
    public function store(CreateAnswerRequest $request, Question $question)
    {
        //
        $question->answers()->create( [

            'body' => $request->body,
            'user_id' => auth()->id()
        ] );

        $question->owner->notify( new NewReplyAdded( $question ) );

        session()->flash( 'success', "Your answer submitted Successfully!!" );
        return redirect( $question->url );
    }

    public function edit(Question $question, Answer $answer)
    {
        //
        $this->authorize('update', $answer);
        return view('answers.edit', compact([
            'question',
            'answer',
        ]) );
    }

    public function update(UpdateAnswerRequest $request, Question $question ,Answer $answer)
    {
        //
        $answer->update( [
            'body' => $request->body
        ] );

        session()->flash( 'success', "Your answer updated Successfully!!" );
        return redirect( $question->url );
    }

    public function destroy(Question $question, Answer $answer)
    {
        //
        $this->authorize('delete', $answer);

        $answer->delete();

        session()->flash( 'success', "Your answer was deleted Successfully!!" );
        return redirect( $question->url );
    }
    
    public function bestAnswer(Request $request, Answer $answer){
        // $this->authorize('markAsBest', $answer);
        $answer->question->markBestAnswer($answer);
        return redirect()->back();
    }
}
