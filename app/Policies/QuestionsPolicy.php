<?php

namespace App\Policies;

use App\Models\Question;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class QuestionsPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {
        //
    } 

    public function view(User $user, Question $question)
    {
        //
    }

    public function create(User $user)
    {
        //
    }

    public function update(User $user, Question $question)
    {
        //
        return $user->id === $question->user_id;
    }

    public function delete(User $user, Question $question)
    {
        //
        return $user->id === $question->user_id && $question->answers_count === 0;
    }

    public function restore(User $user, Question $question)
    {
        //
    }

    public function forceDelete(User $user, Question $question)
    {
        //
    }

    public function markAsFavorite(User $user, Question $question){
        return $user->id != $question->user_id;
    }

    public function vote(User $user, Question $question)
    {
        //
        return $user->id != $question->user_id;
    }
}
