<?php

namespace App\Policies;

use App\Models\Answer;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AnswersPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {
        //
    }

    public function view(User $user, Answer $answer)
    {
        //
    }

    public function create(User $user)
    {
        //
    }

    public function update(User $user, Answer $answer)
    {
        //
        return $user->id === $answer->user_id;
    }

    public function delete(User $user, Answer $answer)
    {
        //
        return $user->id === $answer->user_id && !$answer->is_best;
    }

    public function restore(User $user, Answer $answer)
    {
        //
    }

    public function forceDelete(User $user, Answer $answer)
    {
        //
    }

    public function markAsBest(User $user, Answer $answer){
        return $user->id === $answer->question->user_id;
    }
    
    public function vote(User $user, Answer $answer)
    {
        //
        return $user->id != $answer->user_id;
    }
}
