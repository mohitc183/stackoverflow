<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Policies\QuestionsPolicy;
use App\Policies\AnswersPolicy;
use App\Models\Question;
use App\Models\Answer;
// use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
        // 'App\Models\Question' => 'App\Policies\QuestionsPolicy'


        Question::class => QuestionsPolicy::class,
        Answer::class => AnswersPolicy::class,
        
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
        //Creating Gates for authorization
        // Gate::define('update-question', function($user, $question){
        //     return $user->id === $question->user_id;
        // });

        // Gate::define('delete-question', function($user, $question){
        //     return $user->id === $question->user_id && $question->answers_count === 0;
        // });


    }
}
