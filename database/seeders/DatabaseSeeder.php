<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use \App\Models\User;
use \App\Models\Question;
use \App\Models\Answer;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        
        
        // User::factory()->count(5)->create()->each( function($user){

        //     for($i = 0; $i<=rand(5,10); $i++){
        //         $user->questions()->create( Question::factory()->make()->toArray() );
        //     }

        // } );  



        User::factory()->count(5)->create()
                ->each( function($user){
                    
                    $user->questions()
                            ->saveMany( Question::factory()->count( rand(2 , 5) )->make())
                            ->each( function( $question ){
                                $question->answers()->saveMany( Answer::factory()->count( rand(2, 7) )->make() );
                            } );
                } );  
    }
}
